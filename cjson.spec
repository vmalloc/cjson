Name:           cjson
Version:        1.7.15
Release:        1
Summary:        Ultralightweight JSON parser in ANSI C
 
License:        MIT and ASL 2.0
URL:            https://github.com/DaveGamble/cJSON
Source0:        https://github.com/DaveGamble/cJSON/archive/refs/tags/v1.7.15.tar.gz
 
BuildRequires:  gcc
BuildRequires:  cmake

%description
cJSON aims to be the dumbest possible parser that you can get your job
done with. It's a single file of C, and a single header file.
 
%package devel
Summary:        Development files for cJSON
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       pkgconfig
  
%description devel
The cjson-devel package contains libraries and header files for
developing applications that use cJSON.
  
%prep
%autosetup -n cJSON-%{version}

%build
%cmake
%make_build

%install
%make_install
rm -f %{buildroot}%{_libdir}/*.{la,a}
rm -f %{buildroot}%{_libdir}/cmake/cJSON/*.cmake

%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig 

%files
%license LICENSE
%doc README.md
%{_libdir}/libcjson*.so.*
 
%files devel
%doc CHANGELOG.md CONTRIBUTORS.md
%{_libdir}/libcjson.so
%{_libdir}/pkgconfig/libcjson.pc
%{_includedir}/cjson/

%changelog
* Thu Sep 23 2021 jiangxinyu <jiangxinyu@kylinos.cn> - 1.7.15-1
- Package Init